function problem2(inventory){
    let lastCarDetails = [];
    if(Array.isArray(inventory) == false || inventory == undefined || inventory.length == 0){
        return lastCarDetails;
    }
    lastCarDetails = inventory[inventory.length - 1];
    return lastCarDetails;
}

module.exports = problem2;