function problem4(inventory){
    let carYears = [];
    if(Array.isArray(inventory) == false || inventory == undefined || inventory.length == 0){
        return carYears;
    }
    for(let index=0; index<inventory.length; index++){
        carYears.push(inventory[index].car_year);
    }
    return carYears;
}

module.exports = problem4;