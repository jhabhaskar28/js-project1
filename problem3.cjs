function problem3(inventory){
    let carModels = [];
    if(Array.isArray(inventory) == false || inventory == undefined || inventory.length == 0){
        return carModels;
    }
    for(let index=0; index<inventory.length;index++){
        carModels.push(inventory[index].car_model);
    }
    carModels.sort(
        function(name1, name2) {
          if (name1.toLowerCase() < name2.toLowerCase()){
            return -1;
          } 
          if (name1.toLowerCase() > name2.toLowerCase()){
            return 1;
          }
          return 0;
        }
    );
    return carModels;
}

module.exports = problem3;