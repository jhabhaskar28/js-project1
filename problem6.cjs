function problem6(inventory){
    let BMWAndAudi = [];
    if(Array.isArray(inventory) == false || inventory == undefined || inventory.length == 0){
        return JSON.stringify(BMWAndAudi);
    }
    for(let index=0; index<inventory.length; index++){
        if(inventory[index].car_make === "Audi" || inventory[index].car_make === "BMW"){
            BMWAndAudi.push(inventory[index]);
        }
    }
    return JSON.stringify(BMWAndAudi);
}

module.exports = problem6;