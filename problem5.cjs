function problem5(inventory){
    let olderCars = [];
    if(Array.isArray(inventory) == false || inventory == undefined || inventory.length == 0){
        return olderCars;
    }
    for(let index=0; index<inventory.length; index++){
        if(inventory[index].car_year < 2000){
            olderCars.push(inventory[index].id);
        }
    }
    return olderCars;
}

module.exports = problem5;